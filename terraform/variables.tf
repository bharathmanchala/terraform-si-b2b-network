variable "aws_region" {
    description = "aws region"
	type        = string
}

variable "aws_access_key" {
    description = "access key"
	type        = string
} 

variable "aws_secret_key" {
    description = "secret key"
	type        = string
}
variable "ami" {
    description = "ami id"
	type        = string
}

variable "instance_type" {
    description = "type of instace"
	type        = string
}

variable "monitoring" {
    description = "enable monitoring"
	type        = bool
}

variable "vpc_security_group_ids"{
    description = "vpc security group id"
	type        = string
}

variable "subnet_id" {
    description = "subnet id"
	type        = string
}

variable "user" {
    description = "db generic user"
	type        = string
}

variable "key" {
    description = "pem key"
    type        = any
}

variable "awsscript" {
    description = "aws script"
	type        = any
}

 variable "script" {
    description = "si installation script"
	type        = any
}