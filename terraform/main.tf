terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.65.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region     = var.aws_region
  profile    = "default"
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
}
resource "aws_instance" "sterling-b2b-server" {
  ami                    = "${var.ami}"
  instance_type          = "${var.instance_type}"
  monitoring             = "${var.monitoring}"
  vpc_security_group_ids = [var.vpc_security_group_ids]
  subnet_id              = "${var.subnet_id}"

provisioner "remote-exec" {
       connection {
	   
    type        = "ssh"
    host        = self.public_ip
    agent       = false
    user        = "${var.user}"
  # password    = "${var.password}   
    private_key = file("${var.key}")
  
  }
     script   = ("${var.awsscript}")
  }

provisioner "remote-exec" {
       connection {

     type        = "ssh"
     host        = self.public_ip
     agent       = false
     user        = "${var.user}"
   # password    = "${var.password}
     private_key = file("${var.key}")
	 
   }
      script   = ("${var.script}")
   }
}